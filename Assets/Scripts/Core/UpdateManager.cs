﻿using System;
using UnityEngine;

namespace SecurityGame.Core
{
    public class UpdateManager : MonoBehaviour, IUpdateManager
    {
        public event Action OnFixedUpdate;
        public event Action OnUpdate;

        private void Awake()
        {
            ManagerHolder.I.AddManager(this);
        }

        private void Update()
        {
            OnUpdate?.Invoke();
        }
        private void FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }

    }
}