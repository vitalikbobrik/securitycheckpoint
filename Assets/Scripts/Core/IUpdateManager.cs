﻿using System;

namespace SecurityGame.Core
{
    public interface IUpdateManager
    {
        event Action OnFixedUpdate;
        event Action OnUpdate; 
    }
}