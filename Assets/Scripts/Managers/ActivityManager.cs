﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActivityManager
{
    public static void UpdatePlayerLevel()
    {
        PlayerPrefs.SetInt("level__", GetPlayerLevel() + 1);
    }

    public static int GetPlayerLevel()
    {
        return PlayerPrefs.GetInt("level__", 0);
    }

    public static bool SoundEnabled() => PlayerPrefs.GetInt("sound", 1) == 1? true : false;
    public static bool MusicEnabled() => PlayerPrefs.GetInt("music", 1) == 1 ? true : false;

}

