﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SecurityGame.Core;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Button _yesBtn;
    [SerializeField] private Button _noBtn;

    [SerializeField] private CanvasGroup _bottomButtons;

    [SerializeField] private CanvasGroup _winPanel;
    [SerializeField] private CanvasGroup _losePanel;

    public Button YesBtn
    {
        get { return _yesBtn; }
    }

    public Button NoBtn
    {
        get { return _noBtn; }
    }

    [SerializeField] private Image _upImage;
    [SerializeField] private float _animationSpeed = 1f;

    [SerializeField] private TextMeshProUGUI _levelCount;
    [SerializeField] private TextMeshProUGUI _levelDescription;

    [SerializeField] private RectTransform _progressRoot;
    [SerializeField] private Image _progressImg;


    public TextMeshProUGUI LevelCount
    {
        get { return _levelCount; }
    }

    public TextMeshProUGUI LevelDescription
    {
        get { return _levelDescription; }
    }

    private Sequence _buttonAnimation;

    #region EventsSubscription

    private void OnEnable()
    {
        EventHandler.OnLevelStarted += ShowStartScreen;
        EventHandler.OnFindResult += ScaleButtons;
    }

    public void FadeAndActivateText(TextMeshProUGUI textMeshProUGUI, string text)
    {
        textMeshProUGUI.DOFade(0, _animationSpeed / 5).OnComplete(() =>
        {
            textMeshProUGUI.text = text;
            textMeshProUGUI.DOFade(1, _animationSpeed / 5);
        });
        
    }

    private void ScaleButtons()
    {
        _noBtn.transform.localScale = new Vector3(1.05f, 1.05f, 1.05f);
        _buttonAnimation = DOTween.Sequence();
        _buttonAnimation.SetLoops(-1, LoopType.Yoyo);
        _buttonAnimation.Append(_yesBtn.GetComponent<RectTransform>().DOScale(1.05f, 0.5f));
        _buttonAnimation.Join(_noBtn.GetComponent<RectTransform>().DOScale(1f, 0.5f));
    }

    private void OnDisable()
    {
        EventHandler.OnLevelStarted -= ShowStartScreen;
        EventHandler.OnFindResult -= ScaleButtons;
        DOTween.KillAll();
        RemoveButtonsListeners();
    }
    #endregion


    private void Awake()
    {
        ManagerHolder.I.AddManager(this);
    }

    private void RemoveButtonsListeners()
    {
        _yesBtn.onClick.RemoveAllListeners();
        _noBtn.onClick.RemoveAllListeners();
    }

    private void ShowStartScreen()
    {
        _upImage.DOFade(1, _animationSpeed).SetEase(Ease.OutBounce);
        ShowBottomButtons(true);
    }

    public void UpdateProgressBar(float value)
    {
        _progressImg.DOFillAmount(value, _animationSpeed);
    }

    public void ShowBottomButtons(bool show)
    {
        if (show)
        {
            _bottomButtons.DOFade(1, _animationSpeed);
            _bottomButtons.GetComponent<RectTransform>().DOAnchorPosY(100, _animationSpeed).OnComplete(() =>
            {
                _progressRoot.GetComponent<CanvasGroup>().DOFade(1, _animationSpeed);
                ShowLevelDescription(true);
            });
        }
        else
        {

            _buttonAnimation.Kill();
            RemoveButtonsListeners();
            _bottomButtons.DOFade(0, _animationSpeed);
            _bottomButtons.GetComponent<RectTransform>().DOAnchorPosY(-300, _animationSpeed);
        }
    }

    public void ShowWinPanel()
    {

        ActivityManager.UpdatePlayerLevel();
        ShowLevelDescription(false);
        _winPanel.gameObject.SetActive(true);
        Sequence sequence = DOTween.Sequence();
        sequence.Append(_winPanel.DOFade(1, _animationSpeed / 2));
        sequence.AppendInterval(.5f);
        sequence.Append(_progressRoot.DOAnchorPosY(500, _animationSpeed));
    }

    public void ShowLosePanel()
    {
        _progressRoot.GetComponent<CanvasGroup>().DOFade(0, _animationSpeed);

        ShowLevelDescription(false);
        _losePanel.gameObject.SetActive(true);
        _losePanel.DOFade(1, _animationSpeed/2);
    }

    private void ShowLevelDescription(bool show)
    {
        if (show)
        {
            _levelCount.GetComponent<RectTransform>().DOAnchorPosY(-60, _animationSpeed);
        }
        else
        {
            _levelCount.GetComponent<RectTransform>().DOAnchorPosY(300, _animationSpeed);
        }
    }



}
