﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SecurityGame.Core;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] private FinderMover _finderMover;
    [SerializeField] private LevelContent[] _levelContents;
    [SerializeField] private SpriteRenderer _policeTransform;

    [SerializeField] private ParticleSystem _starParticles;
    [SerializeField] private ParticleSystem _confettiBlast;

    [SerializeField] private SpriteRenderer _characterStatus;
    [SerializeField] private Sprite _sadTexture;
    [SerializeField] private Sprite _angryTexture;
    [SerializeField] private Sprite _happyTexture;

    [SerializeField] private ParticleSystem _winParticles;

    [SerializeField] private AudioSource _backGroundMusic;
    [SerializeField] private AudioClip _winSound;
    [SerializeField] private AudioClip _failSound;
    [SerializeField] private AudioClip _sirenSound;

    private int _playerLevel;

    private UIManager _uIManager;
    private SpriteManager _spriteManager;
    private IUpdateManager _updateManager;

    private int _currentPersonOrder = 0;

    private Person _currentPerson;

    #region EventsSubscription
    private void OnEnable()
    {
        _uIManager = ManagerHolder.I.GetManager<UIManager>();
        _updateManager = ManagerHolder.I.GetManager<IUpdateManager>();
        _spriteManager = ManagerHolder.I.GetManager<SpriteManager>();


        EventHandler.OnFindResult += ResultFinded;
        EventHandler.OnPersonStartFinding += SetButtonValues;
    }


    private void OnDisable()
    {
        EventHandler.OnFindResult -= ResultFinded;
        EventHandler.OnPersonStartFinding -= SetButtonValues;

        ManagerHolder.I.ClearAll();
    }
    #endregion

    private void Awake()
    {
        ManagerHolder.I.AddManager(this);
        _playerLevel = ActivityManager.GetPlayerLevel() % 14;
    }

    private void Start()
    {
        _spriteManager.LoadLevelSprites(_levelContents[_playerLevel].PersonList);
        _uIManager.LevelCount.text = "Level " + (ActivityManager.GetPlayerLevel() + 1).ToString();
        SetButtonValues();
        EventHandler.onLevelStarted();
    }

    private void UpdateHandler()
    {
        Debug.Log("Update activated");

        if (Vector3.Distance(_finderMover.transform.position, _currentPerson.TargetPosition) < 0.4f)
        {
            _characterStatus.sprite = _angryTexture;
            EventHandler.onFindResult();
        }
        else if (Vector3.Distance(_finderMover.transform.position, _currentPerson.TargetPosition) < 2f)
        {
            _characterStatus.sprite = _sadTexture;

        }
        else
        {
            _characterStatus.sprite = _happyTexture;

        }
    }


    

    public void SetButtonValues()
    {
        _characterStatus.sprite = _happyTexture;

        _currentPerson = _levelContents[_playerLevel].PersonList[_currentPersonOrder];
        _uIManager.YesBtn.onClick.RemoveAllListeners();
        _uIManager.NoBtn.onClick.RemoveAllListeners();

        _uIManager.YesBtn.onClick.AddListener(() => SwitchCharacter(_currentPerson.Accept));
        _uIManager.NoBtn.onClick.AddListener(() => SwitchCharacter(!_currentPerson.Accept));
        if (!_currentPerson.Accept)
        {
            _updateManager.OnUpdate += UpdateHandler;
        }
    }


    private void ResultFinded()
    {
        _updateManager.OnUpdate -= UpdateHandler;

        MMVibrationManager.Haptic(HapticTypes.LightImpact);
        //ChangeButtonFeedback();
    }

    private void SwitchCharacter(bool rightAnswer)
    {
        _characterStatus.gameObject.SetActive(false);
        _uIManager.ShowBottomButtons(false);
        _currentPersonOrder++;
        _uIManager.UpdateProgressBar((float)_currentPersonOrder / _levelContents[_playerLevel].PersonList.Count);
        if (rightAnswer)
        {
            ManagerHolder.I.GetManager<FinderMover>().ScaleFinder(true);
            Debug.Log(_currentPersonOrder);
            if (!_levelContents[_playerLevel].PersonList[_currentPersonOrder - 1].Accept)
            {
                

                ShowPolice(true);
            }
            PlayStarParticles();
            
            
            if (_currentPersonOrder == _levelContents[_playerLevel].PersonList.Count)
            {
                _backGroundMusic.DOFade(0, 0.5f).OnComplete(() =>
                {
                    _backGroundMusic.clip = _winSound;
                    _backGroundMusic.loop = false;
                    _backGroundMusic.Play();
                });
                
                _winParticles.gameObject.SetActive(true);
                _uIManager.ShowWinPanel();
                Debug.Log("Level Comleted");
            }
            else
            {
                SetButtonValues();
                DOTween.Sequence().AppendInterval(2).AppendCallback(() =>
                {
                    //_updateManager.OnUpdate += UpdateHandler;
                    _characterStatus.gameObject.SetActive(true);
                    _spriteManager.HideFrwdSprite(_currentPersonOrder - 1);
                    ManagerHolder.I.GetManager<FinderMover>().ScaleFinder(false);
                    ShowPolice(false);
                    ManagerHolder.I.GetManager<SpriteManager>().MoveToNextCharacter();
                    _uIManager.ShowBottomButtons(true);
                });
                Debug.Log("Win");
            }
        }
        else
        {
            _backGroundMusic.DOFade(0, 0.5f).OnComplete(() =>
            {
                _backGroundMusic.clip = _failSound;
                _backGroundMusic.loop = false;
                _backGroundMusic.Play();
            });
            _uIManager.ShowLosePanel();
            Debug.Log("Lose");
        }
    }

    private void PlayStarParticles()
    {
        _starParticles.Play();
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    private void ShowPolice(bool show)
    {
        if (show)
        {
            ManagerHolder.I.GetManager<AudioManager>().PlaySound(_sirenSound);
            _policeTransform.DOFade(1, 1);
            _policeTransform.transform.DOMoveX(2.5f, 1).OnComplete(() =>
            {
                if (PlayerPrefs.GetInt("item_" + _levelContents[_playerLevel].PersonList[_currentPersonOrder - 1].itemIndex) == 0)
                {
                    ManagerHolder.I.GetManager<WareHouseManager>().ShowNewItemPanel
                        (true,
                        Resources.Load<Sprite>("New/" + _levelContents[_playerLevel].PersonList[_currentPersonOrder - 1].itemIndex)
                        );
                    _confettiBlast.Play();
                    PlayerPrefs.SetInt
                        ("item_" + _levelContents[_playerLevel]
                        .PersonList[_currentPersonOrder - 1].itemIndex, 1);
                }

            });
        }
        else
        {
            _policeTransform.DOFade(0, 1);
            _policeTransform.transform.DOMoveX(15, 1);

        }
    }


}
