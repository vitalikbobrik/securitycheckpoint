﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using SecurityGame.Core;

public class WareHouseManager : MonoBehaviour
{
    [SerializeField] private GameObject _itemPrefab;
    [SerializeField] private Transform _wareHouseRoot;
    [SerializeField] private CanvasGroup _warePanel;
    [SerializeField] private Button _warehouseBtn;
    [SerializeField] private Button _closeBtn;

    [Header("NewItemPanel")]
    [SerializeField] private CanvasGroup _newItemPanel;
    [SerializeField] private Image _openedItemImg;
    [SerializeField] private Button _closeNewItemPanelBtn;
    private void OnEnable()
    {
        _warehouseBtn.onClick.AddListener(() => OpenWareHouse(true));
        _closeBtn.onClick.AddListener(() => OpenWareHouse(false));
        _closeNewItemPanelBtn.onClick.AddListener(() => ShowNewItemPanel(false, null));
    }

    private void OnDisable()
    {
        _warehouseBtn.onClick.RemoveAllListeners();
        _closeBtn.onClick.RemoveAllListeners();
        _closeNewItemPanelBtn.onClick.RemoveAllListeners();
    }
    private void Start()
    {
        ManagerHolder.I.AddManager(this);
        for (int i = 1; i < 33; i++)
        {
            var item = Instantiate(_itemPrefab, _wareHouseRoot);
            if(PlayerPrefs.GetInt("item_" + i) == 1)
            {
                item.GetComponent<Image>().sprite = Resources.Load<Sprite>("Items/" + i);
            }
        }
    }

    private void UpdateWareHouse()
    {
        for (int i = 0; i < _wareHouseRoot.childCount; i++)
        {
            if (PlayerPrefs.GetInt("item_" + (i + 1).ToString()) == 1)
            {
                _wareHouseRoot.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Items/" + (i+1).ToString());
            }
            
        }
    }

    private void OpenWareHouse(bool open)
    {
        if (open)
        {
            UpdateWareHouse();
            _warePanel.gameObject.SetActive(true);
            _warePanel.DOFade(1, 0.3f);
        }
        else
        {
            _warePanel.DOFade(0, 0.3f).OnComplete(() =>
            {
                _warePanel.gameObject.SetActive(false);
            });
        }
    }

    public void ShowNewItemPanel(bool show, Sprite sprite)
    {
        if (show)
        {
            _newItemPanel.gameObject.SetActive(true);
            _newItemPanel.DOFade(1, 0.2f);
            _openedItemImg.sprite = sprite;
            _openedItemImg.rectTransform.DORotate(new Vector3(0, 0, 360), .5f, RotateMode.FastBeyond360);
            _closeNewItemPanelBtn.GetComponent<RectTransform>().DOScale(1.1f, .3f).SetLoops(-1, LoopType.Yoyo);
        }

        else
        {
            _newItemPanel.DOFade(0, 0.2f).OnComplete(() => _newItemPanel.gameObject.SetActive(false));
        }
    }
}
