﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SecurityGame.Core;
using DG.Tweening;
public class SpriteManager : MonoBehaviour
{
    [SerializeField] private GameObject _forwardSprite;
    [SerializeField] private GameObject _backSprite;


    [SerializeField] private Transform _spriteRoot;



    private void Awake()
    {
        ManagerHolder.I.AddManager(this);
    }

    public void LoadLevelSprites(List<Person> people)
    {
        for (int i = 0; i < people.Count; i++)
        {
            LoadSprite(i, people[i].name);
        }
    }

    public void LoadSprite(int index, string name)
    {
        GameObject parent = new GameObject("SpriteRoot_" + index);
        parent.transform.SetParent(_spriteRoot);

        var _fSprite = GameObject.Instantiate(_forwardSprite, parent.transform);
        _fSprite.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Level_" + name + "_1");
        _fSprite.transform.position = new Vector3(index * 15, 0, 0);


        var _bSprite = GameObject.Instantiate(_backSprite, parent.transform);
        _bSprite.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Level_" + name + "_2");
        _bSprite.transform.position = new Vector3(index * 15, 0, 0);
    }

    public void MoveToNextCharacter()
    {
        _spriteRoot.DOMoveX(_spriteRoot.transform.position.x - 15, 1);
    }

    public void HideFrwdSprite(int index)
    {
        _spriteRoot.GetChild(index).GetChild(0).GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.None;
        _spriteRoot.GetChild(index).GetChild(1).GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.None;
        _spriteRoot.GetChild(index).GetChild(0).gameObject.SetActive(false);
    }
}
