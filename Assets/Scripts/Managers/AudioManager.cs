﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SecurityGame.Core;
using UnityEngine.UI;
using DG.Tweening;


public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioClip _btnSound;
    [SerializeField] private AudioClip _sirenSound;
    [SerializeField] private AudioSource _backGroudMusic;
    [SerializeField] private Button _soundBtn;
    [SerializeField] private Button _musicBtn;
    [SerializeField] private Button _settingsBtn;
    public static bool SoundOn;
    public static bool MusicOn;
    public bool SettingsOpened = false;
    [SerializeField] private float _animationSpeed = 0.2f;

    private AudioSource _audioSource;
    private void OnEnable()
    {
        _settingsBtn.onClick.AddListener(()=> OpenSoundMenu());
    }
    private void OnDisable()
    {
        _settingsBtn.onClick.RemoveAllListeners();
    }
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        ManagerHolder.I.AddManager(this);
        SoundOn = ActivityManager.SoundEnabled();
        MusicOn = ActivityManager.MusicEnabled();
        if (SoundOn)
        {
            _soundBtn.GetComponent<Image>().DOFade(1, 0);
        }
        else{
            _soundBtn.GetComponent<Image>().DOFade(0, 0);

        }
        if(MusicOn)
        {
            _backGroudMusic.Play();
            _musicBtn.GetComponent<Image>().DOFade(1, 0);
        }
        else
        {
            _musicBtn.GetComponent<Image>().DOFade(0, 0);
        }
    }

    public void PlaySound(AudioClip clip)
    {
        if(SoundOn){
        _audioSource.clip = clip;
        _audioSource.Play();
        }
    }

    public void ChangeSound()
    {
        SoundOn = !SoundOn;
        if(SoundOn)
        {
            PlayerPrefs.SetInt("sound", 1);

            _soundBtn.GetComponent<Image>().DOFade(1, 0.2f);
            _audioSource.DOFade(1, 0.5f);
        }
        else
        {
            PlayerPrefs.SetInt("sound", 0);
            _audioSource.DOFade(0, 0.5f);
            _soundBtn.GetComponent<Image>().DOFade(0, 0.2f);
        }
    }

    public void ChangeMusic(){
        MusicOn = !MusicOn;
            if(MusicOn)
            {
            PlayerPrefs.SetInt("music", 1);
            _backGroudMusic.DOFade(1, 0.5f);
                _backGroudMusic.Play();
                _musicBtn.GetComponent<Image>().DOFade(1, 0.2f);
            }
        else
        {
            PlayerPrefs.SetInt("music", 0);

            _backGroudMusic.DOFade(0, 0.5f);
            _musicBtn.GetComponent<Image>().DOFade(0, 0.2f);
        }
    }

    public void OpenSoundMenu()
    {
        SettingsOpened = !SettingsOpened;
        if(SettingsOpened)
        {        
            _soundBtn.transform.parent.GetComponent<RectTransform>()
            .DORotate(new Vector3(0,0,-360), _animationSpeed * 2, RotateMode.FastBeyond360);
            _musicBtn.transform.parent.GetComponent<RectTransform>()
            .DORotate(new Vector3(0,0,-360), _animationSpeed * 2, RotateMode.FastBeyond360);

            _soundBtn.transform.parent.GetComponent<RectTransform>().DOAnchorPosY(-120, _animationSpeed);
            _musicBtn.transform.parent.GetComponent<RectTransform>().DOAnchorPosY(-240, _animationSpeed);
        }
        else
        {
            _soundBtn.transform.parent.GetComponent<RectTransform>()
            .DORotate(Vector3.zero, _animationSpeed * 2, RotateMode.FastBeyond360);
            _musicBtn.transform.parent.GetComponent<RectTransform>()
            .DORotate(Vector3.zero, _animationSpeed * 2, RotateMode.FastBeyond360);

            _soundBtn.transform.parent.GetComponent<RectTransform>().DOAnchorPosY(0, _animationSpeed);
            _musicBtn.transform.parent.GetComponent<RectTransform>().DOAnchorPosY(0, _animationSpeed);
        }
    }
}
