﻿
using TMPro;
public static class EventHandler
{
    public delegate void GameEvent();

    public static event GameEvent OnLevelStarted;
    public static event GameEvent OnPersonStartFinding;
    public static event GameEvent OnFindResult;
    public static event GameEvent OnButtonClick;
    public static event GameEvent OnSwitchPerson;
    public static event GameEvent OnLevelFinished;
    public static event GameEvent OnLevelFailed;


    public delegate void UIEvent(string count, string desc);
    public static event UIEvent OnSetText;

    public delegate void LevelCompletedEvent(bool completed);
    public static event LevelCompletedEvent OnCompleted;


    public static void onLevelStarted()
    {
        OnLevelStarted?.Invoke();
    }



    public static void onFindResult()
    {
        OnFindResult?.Invoke();
    }

    public static void onCompleted(bool completed)
    {
        OnCompleted?.Invoke(completed);
    }

}
