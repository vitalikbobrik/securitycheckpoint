﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class EmojiAnimation : MonoBehaviour
{
    Sequence sequence;
    private void OnEnable()
    {
        transform.localEulerAngles = new Vector3(0, 0, 10);
        transform.localScale = new Vector3(1,1,1);
        sequence = DOTween.Sequence();
        sequence.SetLoops(-1, LoopType.Restart);
        sequence.Append(transform.DORotate(new Vector3(0, 0, -10), 0.5f).SetLoops(2, LoopType.Yoyo));
        sequence.Join(transform.DOScale(1.02f, 0.25f).SetLoops(4, LoopType.Yoyo));
    }
    private void OnDisable()
    {
        sequence.Kill();
    }
}
