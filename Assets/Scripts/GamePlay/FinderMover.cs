﻿using UnityEngine;
using DG.Tweening;
using SecurityGame.Core;

public class FinderMover : MonoBehaviour
{

    private Vector2 _minPos;
    private Vector2 _maxPos;
    private Vector2 _mousePos;
    private Vector2 _mouseOffset;
    private IUpdateManager _updateManager;
    private UIManager _uiManager;

    [SerializeField] private float _animationSpeed = .5f;

    private void OnEnable()
    {
        EventHandler.OnLevelStarted += SetStartPosition;
        _updateManager = ManagerHolder.I.GetManager<IUpdateManager>();
        _updateManager.OnUpdate += UpdateHandler;
    }

    private void OnDisable()
    {
        EventHandler.OnLevelStarted -= SetStartPosition;
        _updateManager.OnUpdate -= UpdateHandler;
    }


    void Start()
    {
        // Set min and max finder positions
        Vector2 Size = GetComponent<SpriteRenderer>().bounds.extents + new Vector3(-1, 0);
        _minPos = (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(0.1f, 0.2f)) + Size;
        _maxPos = (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(0.9f, 0.85f)) - Size;
        ManagerHolder.I.AddManager(this);
        _uiManager = ManagerHolder.I.GetManager<UIManager>();

    }


    void UpdateHandler()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _mousePos = (Input.mousePosition);
            Vector2 mouseInitialPosition = new Vector2(Camera.main.ScreenToWorldPoint(_mousePos).x, Camera.main.ScreenToWorldPoint(_mousePos).y);
            _mouseOffset = mouseInitialPosition - (Vector2)transform.position;
        }
        else if (Input.GetMouseButton(0))
        {
            _mousePos = (Input.mousePosition);
            Vector2 targetPos = new Vector2(Camera.main.ScreenToWorldPoint(_mousePos).x, Camera.main.ScreenToWorldPoint(_mousePos).y);
            targetPos.x = Mathf.Clamp(targetPos.x - _mouseOffset.x, _minPos.x, _maxPos.x);
            targetPos.y = Mathf.Clamp(targetPos.y - _mouseOffset.y, _minPos.y, _maxPos.y);
            transform.position = targetPos;
        }
    }

    private void SetStartPosition()
    {
        transform.DOMove(new Vector2(2, 5), _animationSpeed);
        transform.DORotate(new Vector3(0, 0, 360), _animationSpeed, RotateMode.FastBeyond360);
    }

    public void ScaleFinder(bool scaleUp)
    {
        Sequence seq = DOTween.Sequence();
        if (scaleUp)
        {
            seq.Append(transform.DOMove(new Vector2(10, -7), _animationSpeed));
            seq.Join(transform.GetComponent<SpriteRenderer>().DOFade(0, _animationSpeed));
            seq.Join(transform.DOScale(20, _animationSpeed));
        }
        else
        {
            seq.Append(transform.DOScale(1, 0));
            seq.Join(transform.DOMove(new Vector2(2, 5), _animationSpeed));
            seq.Join(transform.GetComponent<SpriteRenderer>().DOFade(1, 0));
        }
    }
}

