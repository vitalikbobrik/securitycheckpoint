﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;


[System.Serializable]
public struct Person
{
    public string name;
    public bool Accept;
    public Vector2 TargetPosition;
    public string itemIndex;
}

[CreateAssetMenu(fileName = "LevelContent", menuName = "LevelSettings")]

public class LevelContent : ScriptableObject
{
    public List <Person> PersonList;
}
